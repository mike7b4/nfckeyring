#include "configs.h"

Configs::Configs(QDeclarativeView *_view) :
    QObject((QObject *)NULL)
{
    view=_view;
    connect(view, SIGNAL(statusChanged(QDeclarativeView::Status)), this, SLOT(onReady(QDeclarativeView::Status)));
}

void Configs::setupAndLoad()
{
    QGraphicsObject*root;
    root=view->rootObject();
    QSettings settings(CONFIG_FILENAME, QSettings::IniFormat);
    QString str=settings.value("WallServer/IP").toString();
    int port=settings.value("WallServer/Port").toInt();
    if (str=="")
    {
        settings.setValue("WallServer/IP", (char*)CONFIG_REMOTE_IP);
        settings.setValue("WallServer/Port", CONFIG_REMOTE_PORT);
    }

    root->setProperty("strIp", str);
    root->setProperty("iPort", port);
}

void Configs::setValue(const QString &key, const QString &value)
{
    QSettings settings(CONFIG_FILENAME, QSettings::IniFormat);
    settings.setValue(key, value);
}

void Configs::onReady(QDeclarativeView::Status status)
{
    if (status==QDeclarativeView::Ready)
        setupAndLoad();
}
