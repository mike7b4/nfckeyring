#ifndef NFCACCESS_H
#define NFCACCESS_H


#include <QNearFieldManager>
#include <QObject>

using namespace QtMobility;

class NfcAccess : public QNearFieldManager
{
    Q_OBJECT
    QString strSNR;

public:
    explicit NfcAccess(QObject *parent = 0);

signals:
    void tagFound();
public slots:
    QString getData(){ return strSNR; };
    void onTarget(QNearFieldTarget *target);
};

#endif // MOBILE

