#ifndef CONFIGS_H
#define CONFIGS_H

#include <QObject>
#include <QGraphicsObject>
#include <QDeclarativeView>
#include <QSettings>
#include <QDir>
//#define OSS_DEVELOPER 1

#define CONFIG_XML_FILE CONFIG_PATH"nfckeyring.xml.crypt"
#define CONFIG_FILENAME CONFIG_PATH"nfckeyring.ini"
#define CONFIG_PATH QDir::homePath()+"/.config/nfckeyring/"
#define CONFIG_ICON_PATH CONFIG_PATH+"icons/"
#define MOBILE 1
/* default port */
#define CONFIG_REMOTE_PORT 4444
#define CONFIG_REMOTE_IP "192.168.1."

class Configs : public QObject
{
    Q_OBJECT
private:
    QDeclarativeView *view;
public:
    explicit Configs(QDeclarativeView *view);
    void setupAndLoad();
signals:
public slots:
    void setValue(const QString &key, const QString &value);
    void onReady(QDeclarativeView::Status status);
};

#endif // CONFIGS_H
