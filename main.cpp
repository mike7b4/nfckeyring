#include <QtGui/QApplication>
#include <QDeclarativeEngine>
#include <QDeclarativeComponent>
#include <QDeclarativeContext>
#include <QDebug>
#include <iostream>
#include <openssl/blowfish.h>
#include "qclipboardadapter.h"
#include "qmlapplicationviewer/qmlapplicationviewer.h"
#include "mccryptoengine.h"
#include "configs.h"
#ifdef MOBILE
#include "nfcaccess.h"
#endif

void msgHandler( QtMsgType type, const char* msg )
{
    const char symbols[] = { 'I', 'E', '!', 'X' };
    QString output = QString("[%1] %2").arg( symbols[type] ).arg( msg );
    std::cerr << output.toStdString() << std::endl;
    if( type == QtFatalMsg ) abort();
}

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    qInstallMsgHandler( msgHandler );
    QScopedPointer<QApplication> app(createApplication(argc, argv));

//    QApplication app(argc, argv);

    qmlRegisterType<QmlClipboardAdapter>("meeclipboard", 1, 0, "QClipboard");

    QmlApplicationViewer viewer;
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    MCCryptoEngine *crypto=new MCCryptoEngine();
    Configs *configs=new Configs(&viewer);
    viewer.rootContext()->setContextProperty("crypt", crypto);
    viewer.rootContext()->setContextProperty("configs", configs);
#ifdef MOBILE
    viewer.rootContext()->setContextProperty("Nfc", crypto->nfc);
#else
    viewer.rootContext()->setContextProperty("Nfc", new QObject());
#endif
//    qDebug() << "qmlinit";
    viewer.setSource(QUrl("qrc:/qml/nfckeyring/main.qml"));

//    qmlRegisterType<MyClass>("ScSms", 1,0, "MyClass");
 //  MyClass *party = qobject_cast<MyClass *>(viewer.engine() );
    viewer.showExpanded();
  //  configs->setupAndLoad();
    return app->exec();
    delete configs;
    delete crypto;

}
