#include <QDebug>
#include "qnwalletclient.h"

QNWalletClient::QNWalletClient(QString strip,short port) :
    QSslSocket(NULL),
    strIp(strip),
    iPort(port),
    isConnected(false),
    strData(""),
    strIncoming(""),
    timer()
{
//     connect(this, SIGNAL(encrypted()), this, SLOT(onReady()));
     connect(this, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(onSslErrors(QList<QSslError>)));
     connect(this, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError(QAbstractSocket::SocketError)));
     connect(this, SIGNAL(connected()), this, SLOT(onReady()));
     connect(&timer, SIGNAL(timeout()), this, SLOT(timeoutCheckIncoming()));
}


void
QNWalletClient::timeoutCheckIncoming()
{
    qDebug() << "read";
    if(bytesAvailable())
    {
        QByteArray ar;
        ar=readAll();
        strIncoming.append(ar);
        qDebug() << "rxdata";
        if (strIncoming.indexOf("</items>")!=-1) {
            emit gotData(strIncoming);
            strIncoming="";
        }
    }

}


void
QNWalletClient::onReady()
{
    qDebug() << "ready";
    isConnected=true;
    if (strData.length()) {
        write(QByteArray(strData.toAscii()));
        strData="";
    }
    timer.start(50);
}

void
QNWalletClient::onSslErrors ( const QList<QSslError> & errors )
{
    qDebug() << "sslerror" << errors;
}

void
QNWalletClient::onError ( QAbstractSocket::SocketError err )
{
    timer.stop();
    isConnected=false;
    qDebug() << "ip: " << strIp << err;

}

void
QNWalletClient::send(QString str)
{
    if (!isConnected) {
        strData=str;
        /* FIXME this is a dirty motherfucker */
        abort();
        qDebug() << "connectTo: " << strIp ;
        connectToHost(strIp, iPort);
    }
    else
        write(QByteArray(str.toAscii()));
}
