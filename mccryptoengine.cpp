/* TODO add GPL2 license
   Copyright(C) 2011 - Mikael Hermansson <mike7b4 gmail com>
*/

#include <stdint.h>
#include <openssl/aes.h>
#include <openssl/blowfish.h>
#include <QFile>
#include <QDir>
#ifdef MOBILE
#include <QSystemDeviceInfo>
#include <QSystemInfo>
#endif
#include <QSslSocket>
#include <QWebView>
#include <QDebug>
#include <QUrl>
#include <openssl/sha.h>
#include <openssl/md5.h>
#include "configs.h"
#include "mccryptoengine.h"

#ifdef MOBILE
using namespace QtMobility;
#endif


bool groupCaseInsensitiveLessThan(const CryptoElement *s1, const CryptoElement *s2)
{
    return s1->sGroup.toLower() < s2->sGroup.toLower();
}



MCCryptoEngine::MCCryptoEngine(QObject *parent)
    :QObject(parent),
      xmlarray(),
    stream(&xmlarray),
    elements(),
      client(NULL)
{
    QDir dir;
    dir.mkpath(CONFIG_PATH);
    dir.mkpath(CONFIG_ICON_PATH);

#ifdef MOBILE
    nfc=new NfcAccess();
#endif
}

void
MCCryptoEngine::nfcActivate()
{
#ifdef MOBILE

    nfc->startTargetDetection();
#endif
}

void
MCCryptoEngine::nfcDeactivate()
{
#ifdef MOBILE
    nfc->stopTargetDetection();
#endif
}

void
MCCryptoEngine::logout()
{
    m_key.clear();
    xmlarray="";
    elements.clear();
#ifdef MOBILE
    nfc->startTargetDetection();
#endif
}

bool
MCCryptoEngine::login(QString snr)
{
    QString key;
#ifdef MOBILE
    QSystemDeviceInfo sysm;
    QSystemInfo sys;

    /* create key from IMEA and SNR */

    /* we store file encrypted with bluefish using IMEI or MAC adress as key for now */
    if (sysm.imei().length()==0) {
        key=QString(snr+"89ABCDEF");
        m_key=QByteArray(QString().toAscii().data());
    }
    else {
       key=sysm.imei();
       key=snr+key;
       SHA256_CTX sctx;
       SHA256_Init(&sctx);
       uint8_t skey[32];
       SHA256_Update(&sctx, key.data(), key.length());
       SHA256_Final(skey, &sctx);
       m_key=QByteArray((char*)skey, 32);
       qWarning() << m_key.toHex() << key << endl;
//       qDebug() << m_key.toHex().toUpper();
    }
#else
    /* if were not on mobile we must know the full TAGSNR+IMEA and merge them together */
    key=QString(snr);
    SHA256_CTX sctx;
    SHA256_Init(&sctx);
    uint8_t skey[32];
    SHA256_Update(&sctx, key.data(), key.length());
    SHA256_Final(skey, &sctx);
    m_key=QByteArray((char*)skey, 32);

#endif
    /* if login fail the XML is corrupted and parse XML will return FALSE
        if file loader or file was empty/not found we return TRUE  */
    if (!parseXml()) {
        qWarning() << "nfc: activate" ;
        nfc->startTargetDetection();
        return false;
    }

    /* no file loaded? well we setup some site we know most of the meego users is using :-) */
    if (elements.length()==0)
    {
        qDebug("first creation of file");
#ifdef OSS_DEVELOPER
        elements.append(new CryptoElement("Forums", "http://www.maemo.org", "user", "password"));
        elements.append(new CryptoElement("Forums", "http://www.meego.com", "user", "password"));
        elements.append(new CryptoElement("Forums", "http://developer.nokia.com", "user", "wp7mustdie"));
#endif
        /* Sweden loves N9xx and pissing on WP7, thats why we have a local meego community site so we add it to the list ;) */
#ifdef MOBILE
        if (sys.currentCountryCode().compare("SE")==0)
            elements.append(new CryptoElement("Forums", "http://www.meegosweden.com", "user", "elopfail"));
#endif
        elements.append(new CryptoElement("NFCKeyring", "NFCKeyring", "n9", key));

        buildTree();
       save();

       /* reread saved file to feed xmlarray cause QML is using it instead of elements */
       parseXml();
    }
    else {
        addNew("NFCKeyring", "NFCKeyring", "n9", key);

    }

    return true;
}

QString
MCCryptoEngine::getIconFromSite(QString site)
{
    uint8_t result[16];
    MD5((const uint8_t*)site.data(), site.size(),result);
    QString sIconUrl=CONFIG_ICON_PATH+QByteArray((char*)result).toHex()+".png";
//    IconMan*frame=new IconMan(QUrl(site), sIconUrl);

    return sIconUrl;
}

void
MCCryptoEngine::buildTree(QString group)
{
    xmlarray="";
    stream.setAutoFormatting(true);
    stream.writeStartDocument();
    stream.writeStartElement("items");
    foreach(CryptoElement *ptr, elements)
    {
        qDebug() << ptr->sGroup << "?" << group;
        if (group!="" &&
            group!=ptr->sGroup)
            continue;

        uint8_t result[16];
        MD5((const uint8_t*)ptr->sSite.data(), ptr->sSite.size(),result);
        ptr->sIconUrl=CONFIG_ICON_PATH+QByteArray((char*)result).toHex()+".png";



        stream.writeStartElement("item");

        stream.writeStartElement("site");
        stream.writeCharacters(ptr->sSite);
        /* </site> */
        stream.writeEndElement();

        stream.writeStartElement("iconurl");
        stream.writeCharacters(ptr->sIconUrl);
        /* </IconUrl> */
        stream.writeEndElement();


        stream.writeStartElement("user");
        stream.writeCharacters(ptr->sUser);
        /* </user> */
        stream.writeEndElement();

        stream.writeStartElement("password");
        stream.writeCharacters(ptr->sPass);
        /* </password> */
        stream.writeEndElement();

        stream.writeStartElement("group");
        stream.writeCharacters(ptr->sGroup);
        /* </group> */
        stream.writeEndElement();


        /* </item> */
        stream.writeEndElement();
    }
    /* </items> */
    stream.writeEndElement();
    /* </xml> */
    stream.writeEndDocument();


}

bool
MCCryptoEngine::parseXml()
{
    QString group,site,user,pass,desc;
    QXmlStreamReader xml;
    elements.clear();
    xmlarray=decryptFromFile(CONFIG_XML_FILE);
    return parseXml(xmlarray);
}

bool
MCCryptoEngine::parseXml(QString xmla)
{
     QString group,site,user,pass,desc;
    uint8_t state=0;
    bool res=false;
    QXmlStreamReader xml;
    elements.clear();
 //   xmlarray=decryptFromFile(CONFIG_XML_FILE);

    /* decryptFromFile is 0 if file not found */
    if(xmla.length()==0)
        return true; /* not an error */

    xml.addData(xmla);
    /* TODO if file is big this should be threaded */
    while (!xml.atEnd()) {
         xml.readNext();

        if (xml.isStartElement())
        {
            if (xml.name()=="item") {
                state=1;
                res=true;
            }
            else if (xml.name()=="site") {
                site=xml.readElementText();
            }
            else if (xml.name()=="user") {
                user=xml.readElementText();

            }
            else if (xml.name()=="password") {
                pass=xml.readElementText();
            }
            else if (xml.name()=="group") {
                group=xml.readElementText();
#ifdef FIX_CHECK_IF_NOT_ASCII
                for(int i=0; i <= group.length(); i++)
                {
                    /* dopes not work FIXME */
                    if (group[i]>127){
                        group="Other";
                        break;
                    }
                }
#endif

            }

        }
        else if (xml.isEndElement())
        {
            if (xml.name()=="item") {
                state=0;
                if (group=="" || group=="forum")
                    group="Other";

                addNew(group, site, user, pass, desc, false);
                site="";
                user="";
                pass="";
                desc="";
                group="";
            }

        }
    }
    return res;
}

QByteArray
MCCryptoEngine::decryptFromFile(const QString &fname)
{
    QByteArray b;
    QFile fil(fname);
    if (fil.open(QIODevice::ReadOnly)) {
        QByteArray in=fil.readAll();
        b=decrypt(in);
        fil.close();
    }

    return b;
}

/*! Encrypts to file
   @param the filename
   @param the data to encrypt
   @param ENC or DEC
*/
void
MCCryptoEngine::encryptToFile(const QString &fname, const QByteArray &data, int enc)
{
    QFile fil(fname);
    if (fil.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        fil.write(encrypt(data, enc));
        fil.close();
    }
    else
        qDebug("Write error\n");
}

/*!set Key from ByteArray
  @param from ByteArray lengt must be 16 bytes
*/
void
MCCryptoEngine::setKey(const QByteArray &key)
{
    m_key=key;
    qDebug("Keylength %d\n%02X", key.length(), m_key.at(0) );
}

void
MCCryptoEngine::setKeyFromHexString(const QString &str)
{
    QByteArray a=QByteArray::fromHex(str.toAscii());
    setKey(a);
}

QByteArray
MCCryptoEngine::encrypt(const QByteArray &data, int enc)
{
    BF_KEY bfkey;
    QByteArray bytearr;
    uint8_t in[8];
    uint8_t out[8];
    uint8_t index=0;
    int8_t pads=0;

    if (m_key.length()==0)
    {
        qWarning("no key loaded op aborted\n");
        return bytearr;
    }
    BF_set_key(&bfkey, 8, (const uint8_t*)m_key.data());

    for (int i=0;i<data.length();i++)
    {
        in[index++]=data.at(i);
        if (index==8)
        {
            index=0;
            BF_ecb_encrypt(in, out, &bfkey, enc );
            for (int b=0;b<8;b++)
                bytearr+=out[b];
        }
    }

    /* we have one more frame we have to pad */
    /* bluefish wants 8 bytes blocks */
    if (index!=0)
    {
        pads = 8-(data.length() % 8);
        while((pads--))
            in[index++]=0;

        BF_ecb_encrypt(in, out, &bfkey,enc );
        for (int b=0;b<8;b++)
            bytearr=bytearr+(out[b]);

    }


    return bytearr;
}

QByteArray
MCCryptoEngine::decrypt(const QByteArray &key)
{
    return encrypt(key, BF_DECRYPT);
}

/* accessable via QML */
void
MCCryptoEngine::save()
{
    encryptToFile(CONFIG_XML_FILE, xmlarray.toUtf8().data());
    emit xmlChanged();

}

/* accessable via QML */
QString
MCCryptoEngine::load(QString key)
{
    (void)key;
    xmlarray=decryptFromFile(CONFIG_XML_FILE);
    printf(" %s\n", xmlarray.toAscii().data());
    return xmlarray;
}

QString
MCCryptoEngine::getXML(QString group)
{
    buildTree(group);
    qDebug() << "==============================================\n" << xmlarray;
    return xmlarray;
}



/* accessable via QML */
bool
MCCryptoEngine::remove(QString group, QString site)
{
    (void)group;
    QDir dir;
    int ind=0;
    foreach(CryptoElement *ptr, elements)
    {
        if (ptr->sSite==site) {
            elements.removeAt(ind);
            delete ptr;
            goto btree;
        }
        ind++;
    }
    /* if not found */
    return false;
btree:
    buildTree();
    dir.mkpath(CONFIG_PATH);
    encryptToFile(CONFIG_XML_FILE, xmlarray.toUtf8().data());
    emit xmlChanged();

    return true;
}

void
MCCryptoEngine::addNew(QString group, QString site, QString user, QString pass, QString desc, bool save)
{
    //QDir dir;

    qSort(elements.begin(), elements.end(), groupCaseInsensitiveLessThan);

    CryptoElement *ptr;
    foreach(ptr, elements)
    {
        if (ptr->sSite==site) {
            qDebug() << "found replacing" << site << user;
            ptr->sPass=pass;
            ptr->sUser=user;
            ptr->sGroup=group;
            ptr->sDescription=desc;
            goto btree;
        }
    }
    /* not found insert new */
    ptr=new CryptoElement(group, site, user, pass, desc);
    elements.append(ptr);
btree:
    QString b(site);
    uint8_t result[16];
    MD5((const uint8_t*)b.data(), b.size(),result);
    ptr->sIconUrl=CONFIG_ICON_PATH+QByteArray((char*)result).toHex()+".png";

    if (!save)
        return ;

    qDebug() << "rebuildtree";
    buildTree();
//    QDir d;
   // dir.mkpath(CONFIG_PATH);
    encryptToFile(CONFIG_XML_FILE, xmlarray.toUtf8().data());
    emit xmlChanged();
}


bool
MCCryptoEngine::sendto(QString ip, short port, QString selsite)
{
    QString xml;
    QXmlStreamWriter sxml(&xml);

    sxml.setAutoFormatting(true);
    sxml.writeStartDocument();
    sxml.writeStartElement("items");
    foreach (CryptoElement *ptr, elements)
    {
        if (selsite==ptr->sSite || selsite=="")
        {
            sxml.writeStartElement("item");

            sxml.writeStartElement("site");
            sxml.writeCharacters(ptr->sSite);
            /* </site> */
            sxml.writeEndElement();

            sxml.writeStartElement("user");
            sxml.writeCharacters(ptr->sUser);
            /* </user> */
            sxml.writeEndElement();

            sxml.writeStartElement("password");
            sxml.writeCharacters(ptr->sPass);
            /* </password> */
            sxml.writeEndElement();


            stream.writeStartElement("group");
            stream.writeCharacters(ptr->sGroup);
            /* </group> */
            stream.writeEndElement();

            /* </item> */
            sxml.writeEndElement();

        }
    }
    /* </items> */
    sxml.writeEndElement();

    sxml.writeEndDocument();
    qDebug() << xml;

    if (client==NULL) {
        client=new QNWalletClient(ip, port);
        connect(client, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onFromClientError(QAbstractSocket::SocketError)));
        connect(client, SIGNAL(gotData(QString)), this, SLOT(onReceivedData(QString)));
    }
    client->send(xml);

    return true;
}

void
MCCryptoEngine::onReceivedData(QString str)
{
    qDebug() << "============ got data ===============";
    qDebug() << str;
    printf("data\n");
    parseXml(str);
    xmlarray=str;
    save();
    qDebug() << "=============got data end=============";
}

void
MCCryptoEngine::onFromClientError(QAbstractSocket::SocketError err)
{
    qDebug() << err;
    emit clientError();
}
