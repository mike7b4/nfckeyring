#ifndef QNWALLETCLIENT_H
#define QNWALLETCLIENT_H

#include <QObject>
#include <QSslSocket>
#include <QTimer>
class QNWalletClient : public QSslSocket
{
    Q_OBJECT
private:
    QString strIp;
    bool isConnected;
    QString strData;
    QTimer timer;
    QString strIncoming;
    short iPort;
public:
    explicit QNWalletClient(QString strIp,short port);
    void send(QString str);
signals:
    void gotData(QString str);
public slots:
    /* user a timer to lazy create a new Thread  */
    void timeoutCheckIncoming();
    void onReady();
    void onSslErrors ( const QList<QSslError> & errors );
    void onError(QAbstractSocket::SocketError e);
};

#endif // QNWALLETCLIENT_H
