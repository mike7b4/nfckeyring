/* TODO add GPL2 license
   Copyright(C) 2011 - Mikael Hermansson <mike7b4 gmail com>
*/

#ifndef MCCRYPTOENGINE_H
#define MCCRYPTOENGINE_H

#include <QObject>
#include <QXmlStreamWriter>
#include <QWebSettings>
#include <QWebView>
#include <openssl/blowfish.h>

#include "qnwalletclient.h"
#include "configs.h"
#ifdef MOBILE
#include "nfcaccess.h"
#endif
#ifdef DISABLED
class IconMan: public QWebView
{
    QString saveto;
    QString url;
    Q_OBJECT
public:
    IconMan(QUrl _url, QString _saveto)
        :QWebView()
    {
        url=_url.toString();
        qDebug()<<_saveto;
        saveto=_saveto;
        connect(this, SIGNAL(loadFinished(bool)), this, SLOT(onDone(bool)));
        load(_url);
    }
    ~IconMan(){ qDebug() << "IconMan::destruct()"; }

public slots:
        void onDone(bool ok)
        {
            qDebug() << "opened: " << url << " " << ok;
            QIcon ico=icon();
            ico.pixmap(64,64).save(saveto);
            deleteLater();
        }

};
#endif

/* this is just a struct holind db */
class CryptoElement
{
    public:

       QString sSite;
       QString sUser;
       QString sPass;
       QString sDescription;
       QString sGroup;
       QString sIconUrl;
       CryptoElement(QString group, QString site, QString user, QString pass, QString desc="")
       {
        sSite=site;
        sUser=user;
        sPass=pass;
        sDescription=desc;
        sGroup=group;
       }
       CryptoElement(QStringRef group, QStringRef site, QStringRef user, QStringRef pass, QStringRef desc)
       {
        sSite=site.toString();
        sUser=user.toString();
        sPass=pass.toString();
        sDescription=desc.toString();
        sGroup=group.toString();
       }
};

class MCCryptoEngine : public QObject
{
    Q_OBJECT

private:
    QString xmlarray;
    QXmlStreamWriter stream;
    QList<CryptoElement*>elements;
    QByteArray m_key;
    QByteArray m_cbc;
    QNWalletClient *client;
public:
    explicit MCCryptoEngine(QObject *parent = 0);
#ifdef MOBILE
    NfcAccess *nfc;
#endif
    void setKey(const QByteArray &key);
    void setKeyFromHexString(const QString & str);
    QByteArray encrypt(const QByteArray &arr, int enc=BF_ENCRYPT);
    QByteArray decrypt(const QByteArray &arr);
    QByteArray decryptFromFile(const QString &fname);
    void encryptToFile(const QString &fname, const QByteArray &bytes, int enc=BF_ENCRYPT);
    void buildTree(QString str="");
    bool parseXml();
    bool parseXml(QString xmla);
public slots:
    /* QML Access this */
    void save();
    QString getXML(QString group);
    QString getIconFromSite(QString site);
    /* deprecatred no use */
    QString load(QString key);
    void addNew(QString group, QString site, QString user, QString pass, QString desc="", bool save=true);
    bool remove(QString group, QString site);
    bool login(QString snr);
    void logout();
    bool sendto(QString ip, short port, QString site);
    void onFromClientError(QAbstractSocket::SocketError);
    void onReceivedData(QString str);
    void nfcActivate();
    void nfcDeactivate();
signals:
    void clientError();
    void xmlChanged();
};

#endif // MCCRYPTOENGINE_H
