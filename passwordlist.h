#ifndef PASSWORDLIST_H
#define PASSWORDLIST_H

#include <QObject>

class PasswordList : public QObject
{
    Q_OBJECT
public:
    explicit PasswordList(QObject *parent = 0);

signals:

public slots:
    bool login(QString pwn);

};

#endif // PASSWORDLIST_H
