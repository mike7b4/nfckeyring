# Add more folders to ship with the application, here
#folder_01.source = qml/qnfcwal
#folder_01.target = qml
#DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
#QML_IMPORT_PATH =

symbian:TARGET.UID3 = 0xE4CBDE54

# Smart Installer package's UID
# This UID is from the protected range and therefore the package will
# fail to install if self-signed. By default qmake uses the unprotected
# range value if unprotected UID is defined for the application and
# 0x2002CCCF value if protected UID is given to the application
#symbian:DEPLOYMENT.installer_header = 0x2002CCCF

# Allow network access on Symbian
symbian:TARGET.CAPABILITY += NetworkServices

# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

# Speed up launching on MeeGo/Harmattan when using applauncherd daemon
CONFIG += qdeclarative-boostable mobility
QT+=network webkit
MOBILITY+=systeminfo connectivity

# Add dependency to Symbian components
# CONFIG += qt-components

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    mccryptoengine.cpp \
    nfcaccess.cpp \
    qclipboardadapter.cpp \
    qnwalletclient.cpp \
    configs.cpp

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog \
    qml/nfckeyring/foo.js \
    qml/nfckeyring/TopPanel.qml \
    qml/nfckeyring/TopButton.qml \
    qml/nfckeyring/Settings.qml \
    qml/nfckeyring/Numbers.qml \
    qml/nfckeyring/NewEntry.qml \
    qml/nfckeyring/MrElop.qml \
    qml/nfckeyring/MainPage.qml \
    qml/nfckeyring/main.qml \
    qml/nfckeyring/ListPasswords.qml \
    qml/nfckeyring/ListDelegate.qml \
    qml/nfckeyring/DialogSharing.qml \
    qml/nfckeyring/AnswerYesNo.qml \
    qml/nfckeyring/AboutPage.qml \
    qml/nfckeyring/GroupsDelegate.qml \
    qml/nfckeyring/ListGroups.qml \
    qml/nfckeyring/ErrorDialog.qml \
    qml/nfckeyring/test.qml

HEADERS += \
    mccryptoengine.h \
    nfcaccess.h \
    qclipboardadapter.h \
    qnwalletclient.h \
    configs.h



LIBS+=-lcrypto

RESOURCES += \
    nfckeyring.qrc






INCLUDEPATH=/usr/include/







contains(MEEGO_EDITION,harmattan) {
    icon.files = nfckeyring.png
    icon.path = /usr/share/icons/hicolor/80x80/apps
    INSTALLS += icon images
    DEFINES+=MOBILE
    images.files = cross.png stripes.png
    images.path = /opt/nfckeyring/images
}


contains(MEEGO_EDITION,harmattan) {
    desktopfile.files = $${TARGET}.desktop
    desktopfile.path = /usr/share/applications
    INSTALLS += desktopfile
    DEFINES+=MOBILE
    images.files = cross.png stripes.png
    images.path = /opt/nfckeyring/images
}



