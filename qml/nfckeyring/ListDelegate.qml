import QtQuick 1.1
import com.nokia.meego 1.0
Item{
    function parseSite(st)
    {
        st=st.replace("http://", "");
        st=st.replace("https://", "");
        return st
    }
    id: delegate
   // clip: true
    height: 60
    width: delegate.ListView.view.width
    Image{
         id: icon
         anchors.leftMargin: 10
         anchors.rightMargin: 10
         anchors.verticalCenter: parent.verticalCenter
         source: "http://"+parseSite(site)+"/favicon.ico"
         width: 32
         height: 32
         fillMode: Image.PreserveAspectFit
    }

     Item {
        id: wrapper
        anchors{
            left: icon.right
            right: parent.right
            leftMargin: 8
            topMargin: 16
        }
        width: parent.width-icon.width-16;
        height: parent.height

        Label {
             id: idSite
             anchors{
                 top: parent.top
             }
          //   anchors.topMargin: 10
             text: parseSite(site);
             width: parent.width;
             wrapMode: Text.WordWrap
             color: "white"
             font {   pixelSize: 30 }
        }
        Item {
            id: child
            width: parent.width
            opacity: 0.0
           // radius: 5
            anchors{
                top: idSite.bottom
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }
            Label {
                 id: labelUser
                 anchors{
                     top: parent.top
                     left: parent.left
                     right: parent.right
                     leftMargin: 8
                     rightMargin: 8

                 }
                 visible: true
    //                 anchors.top: idSite.bottom
                 color: "white"
                 width: parent.width;
                 text: user
                 font { pixelSize: 26 }
             }
             Label {
                 id: labelPassword
                 visible: true
                 anchors{
                     top: labelUser.bottom
                     bottom: parent.bottom
                     left: parent.left
                     right: parent.right
                     leftMargin: 8
                     rightMargin: 8
                 }
                 width: parent.width;
                 text: password
                 color: "white"
                 font {   pixelSize: 26 }
             }
        }
         Item{
            id: pad2
            height: 10
            width: parent.width
         }
         MouseArea{
             anchors.fill: parent
             onClicked:  {
                 /* those vars is used by NewPage*/
                 if (strSite==site)
                     strSite=""
                 else {
                     strGroup=group
                     strSite=site
                     strUser=user
                     strPassword=password
                     strIcon=iconurl
                 }
                 timer.restart()
             }
             onPressAndHold:  {
                 bInSiteEnabled=false;
                 vib.start()
                 pageStack.push(Qt.resolvedUrl("NewEntry.qml"));
                 timer.stop()
             }

         }
         states: State
         {
             name: "selected"
             when: (strSite==site)
             PropertyChanges {target: child; opacity: 0.9; color: "black"}
//             PropertyChanges {target: labelPassword; visible: true; }
        //     PropertyChanges { target: listView; explicit: true; contentY: wrapper.y }
             PropertyChanges { target: delegate;   height: 130 }
         }
         transitions: Transition {
                 ParallelAnimation {
                     ColorAnimation { property: "color"; duration: 500 }
                     NumberAnimation { duration: 300; properties: "x,contentY,height,width" }
                 }
          }
    }
}
