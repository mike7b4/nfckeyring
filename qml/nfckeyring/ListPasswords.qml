import QtQuick 1.1
import com.nokia.meego 1.0
import meeclipboard 1.0
import QtMobility.feedback 1.1
Page {
    orientationLock: Screen.Portrait

    tools:  commonTools
    Connections{
        target: crypt
        onXmlChanged:
        {
            xmlModel.xml=crypt.getXML("");

        }
        onClientError:{
            errorDialog.texttitle="ServerWallet"
            errorDialog.subtitle="Connection failed\nMaybe wrong IP?("+strIp+")\nor server not running?"
            errorDialog.open()
        }
    }

    HapticsEffect {
        id: vib
        attackIntensity: 0.0
        attackTime: 250
        intensity: 1.0
        duration: 100
        fadeTime: 250
        fadeIntensity: 0.0
    }

    QClipboard{
        id: clipboard
    }

    Component {
           id: sectionHeading
           Item {
               width: parent.width
               /* 10 is margin */
               height: childrenRect.height+16

               Label {
                   id: labelText
                   text: section
                   font.bold: true
                   font.italic: false
                   font.pixelSize: 36
                   anchors.topMargin: 10
                   anchors.top: parent.top
                   anchors.left: parent.left
                   anchors.leftMargin: 10
                   color: "cyan"
               }
               Rectangle{
                   anchors.top:labelText.bottom
                   gradient: Gradient {
                         GradientStop { position: 0.0; color: "#333333" }
                         GradientStop { position: 1.0; color: "#777777" }
                     }
                   anchors {
                       right: parent.right
                       left: parent.left
                       rightMargin: 0
                   }
                   height: 3
                   width: parent.width
               }

         }
    }

    ListView {
        id: listView
        spacing: 5
        width: parent.width-32
       // height: mainPage.height//-topPanel.height
        anchors.leftMargin: 16
        anchors.rightMargin: 16
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        model: xmlModel
        delegate: ListDelegate {}
        focus: true
        currentIndex: -1
        section.property: "group"
        section.criteria: ViewSection.FullString
        section.delegate: sectionHeading

    }

    XmlListModel {
        id: xmlModel
        xml: crypt.getXML("")
        query: "/items/item"

        XmlRole { name: "site"; query: "site/string()" }
        XmlRole { name: "user"; query: "user/string()" }
        XmlRole { name: "password"; query: "password/string()" }
        XmlRole { name: "group"; query: "group/string()" }
        XmlRole { name: "iconurl"; query: "iconurl/string()" }
    }

    ErrorDialog{
        id: errorDialog
    }
    DialogSharing{
        id: myDialog
    }

    AnswerYesNo{
        id: askDialog
        texttitle: "Remove:"
        subtitle: strSite+"?"
        onAccepted: { crypt.remove(currentGroup, strSite) }
    }

    ToolBarLayout {
        id: commonTools
        visible: false
        ToolIcon {
            id: buttonBack
            iconId: "toolbar-back";
            anchors.left: (parent === undefined) ? undefined : parent.left
            onClicked: {
                crypt.logout()
                mainPage.state=""
                pageStack.pop();
            }
        }
        ToolIcon {
            id: buttonAdd
            iconId: "toolbar-add";
            anchors.left: buttonBack.right
            onClicked: {
                strSite=""
                strUser=""
                strPassword=""
                strGroup=""
                strIcon=""
                bInSiteEnabled=true
                pageStack.push(Qt.resolvedUrl("NewEntry.qml"))
            }
        }
        ToolIcon {
            id: buttonDelete
            iconId: "toolbar-delete";
            anchors.left: buttonAdd.right
            onClicked: {
                if (strSite!="")
                    askDialog.open()
            }
        }
        ToolIcon {
            id: buttonCopy
            iconId: "toolbar-share";
            anchors.left: buttonDelete.right
            onClicked: { myDialog.open()  }
        }
        ToolIcon {
            id: buttonMenu
            platformIconId: "toolbar-view-menu"
            anchors.right: (parent === undefined) ? undefined : parent.right
            onClicked: (myMenu.status == DialogStatus.Closed) ? myMenu.open() : myMenu.close()
        }
    }



}
