import QtQuick 1.1
import com.nokia.meego 1.0
Item{
    height: 70
    width: parent.width
    z: 1
    property string strLogo: "NFC Keyring v"+version
    Rectangle {
        id: radTop
        height: 10
        width: parent.width
        color: "darkgray"
        anchors.topMargin: 0
        radius: 10
    }
    Rectangle {
        height: 63
        anchors.top: radTop.bottom-3
        color: "darkgray"
        width: parent.width
/*
        gradient: Gradient {
              GradientStop { position: 0.1; color: "darkcyan" }
              GradientStop { position: 1.0; color: "cyan" }
        }
        */
        Label {
            text: strLogo
//            color: "white"
      //      font.pixelSize: 32
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
        }
    }
}
