import QtQuick 1.1
import com.nokia.meego 1.0
import "foo.js" as Foo

Page {
    id: root
    tools: commonTools
    orientationLock: Screen.Portrait

    anchors.topMargin: 0

    function checkIfReadyToLogin()
    {
        /* happens if not reegistered TAG */
        if (strSNR=="")
        {
            strMsg="<i>Failed to read Serial. Try Again by TAP longer.</i>"
            crypt.nfcActivate()
            root.state="error"
            timer.restart()
        }
        else if (crypt.login(strSNR)==false) {
            strMsg="<i>Login Failed :/<br>Try Again!</i>"
          //  colorValue="red"
            strSNR=""
            root.state="error"
            timer.restart()
        }
        else {
            /* autologout if inactive for 90 seconds */
            timer.restart()
            pageStack.push(Qt.resolvedUrl("ListPasswords.qml"));
            root.state=""
        }
    }

    function stateDefault()
    {
        strMsg= "Login by Tap with your NFC Key, Buscard or Mifare compatible Tag";
    }


    Connections{
        target: Nfc
        onTagFound:{
            strSNR=Nfc.getData()
            checkIfReadyToLogin()
        }
    }

    states:[
        State{
            name: "error"
            PropertyChanges{ target: labelPassword; color: "red"; }
        },
        State{
            name: ""
            PropertyChanges{ target: labelPassword; color: "orange"; }
            StateChangeScript{ script: stateDefault(); }
        }
    ]

    /*
    TopPanel{
        id: topPanel
    }
*/
    Item{
        id: item1
        anchors.top: parent.top
        height: parent.height
        width: parent.width

        Column {
            anchors.fill: parent
            anchors.topMargin: 100
            anchors.leftMargin: 16
            anchors.rightMargin: 16
            spacing: 20

            Image{
                source: "qrc:/nfckeyring.png"
                height: 128
                width: 128
                fillMode: Image.PreserveAspectFit
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
                }
            }
            Rectangle{
                gradient: Gradient {
                      GradientStop { position: 0.0; color: "#333333" }
                      GradientStop { position: 1.0; color: "#777777" }
                  }
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                height: 3
                width: parent.width-64
            }

            Text {
                id: labelPassword
                color: colorValue
                font.pixelSize: 48
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: 32
                anchors.rightMargin: 32
                text: strMsg
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                wrapMode: Text.WordWrap
                height: 100

            }
        }
/*
        Column {
            anchors.fill: parent
            anchors.leftMargin: 16
            anchors.rightMargin: 16
            spacing: 20

            Item{
                height: 300
                width: parent.width
                Image{
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    height: 128
                    width:  128
                    source: "/nfckeyring.png"
                    MouseArea{
                        anchors.fill: parent
                        onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
                    }
                }
            }
            Label {
                id: labelPassword
                text: "<center>"+strKey+"</center>"
                color: colorValue
                height: 60
                width: parent.width
                font.pointSize: 20

                anchors.horizontalCenter: parent.horizontalCenter
            }
            Grid {
                id: gridHexChars
                anchors.top: labelPassword.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 5
                columns: 6
                rows: 1
                Numbers{
                    text: "A"
                }
                Numbers{
                    text: "B"
                }
                Numbers{
                    text: "C"
                }
                Numbers{
                    text: "D"
                }
                Numbers{
                    text: "E"
                }
                Numbers{
                    text: "F"
                }
            }
            Grid
            {
                anchors.top: gridHexChars.bottom
                anchors.topMargin: 10
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 5
                columns: 5
                rows: 2

                Numbers{
                    text: "1"
                }
                Numbers{
                    text: "2"
                }
                Numbers{
                    text: "3"
                }
                Numbers{
                    text: "4"
                }
                Numbers{
                    text: "5"
                }
                Numbers{
                    text: "6"
                }
                Numbers{
                    text: "7"
                }
                Numbers{
                    text: "8"
                }
                Numbers{
                    text: "9"
                }
                Numbers{
                    text: "0"
                }
            }
        }
*/
    }

}
