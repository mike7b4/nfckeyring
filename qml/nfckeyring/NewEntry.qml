import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: newPage

    anchors.fill: parent
    orientationLock: PageOrientation.LockPortrait

    TopButton{
        id: topPanel
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        onSubmit: {
            crypt.addNew(inGroup.text, inSite.text, inUser.text, inPassword.text, true);
            strSite=""
            timer.restart()
            pageStack.pop();
         }
        onCancel:{
            timer.restart()
            pageStack.pop()

        }
    }

    Item
    {
      id: container
      anchors.top: topPanel.bottom
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      anchors.right: parent.right
      height: parent.height
      width: parent.width
      Column{
        id: col
        width: container.width
        spacing: 10
        anchors.fill: parent
        anchors.leftMargin: 16
        anchors.rightMargin: 16

 /*
       Rectangle{
            color: "lightsteelblue"
            width: parent.width

            height: buttonCancel.height+10

            Row{
                anchors.fill: parent
                spacing: 20
                Button{
                    id: buttonCancel
                    width: (newPage.width/2)-10
                    text: "Cancel"
                    onClicked: {
                        pageStack.pop();
                        theme.inverted = true
                    }
                }
                Button{
                    id: buttonSave

                    width: (newPage .width/2)-10
                    text: "Save"
                    onClicked: {
                        crypt.addNew(currentGroup, inSite.text, inUser.text, inPassword.text);
                        strSite=""
                        theme.inverted = true
                        timer.restart()
                        pageStack.pop();
                    }
                }
            }
        }
*/
        Label{
            text: "Sitename/title: "
        }
        TextField {
            id: inSite
            enabled: bInSiteEnabled
            placeholderText:  "Site URL/title of this password entry"
            maximumLength: 64
            width: parent.width
            /* if enabled focus else userentry has focus */
            focus: bInSiteEnabled
           // width: newPage.width-32
            inputMethodHints: Qt.ImhUrlCharactersOnly
            text:  strSite
            Keys.onReturnPressed: {
                 inUser.focus = true;
            }
        }


        Label{
            text: "Username: "
        }
        TextField {
            id: inUser
            placeholderText:  "username"
            maximumLength: 32
//            width: newPage.width
            text:  strUser

            /* if insite enabled focus else userentry has focus */
            focus: !bInSiteEnabled

            //height: 40 // UiConstants.FieldTextHeight
            Keys.onReturnPressed: {
                 inPassword.focus = true;
            }
        }
        Label{
            text: "Password: "

        }
        TextField {
            id: inPassword
            placeholderText:  "Password"
            maximumLength: 32
            width: parent.width
            text:  strPassword
            //height: 40 // UiConstants.FieldTextHeight
            Keys.onReturnPressed: {
                 inGroup.focus=true;
            }
        }
        Label{
            text: "Category: "

        }
        TextField {
            id: inGroup
            placeholderText:  "Specify a category"
            maximumLength: 16
            width: parent.width
            text:  strGroup
           // height: 40 // UiConstants.FieldTextHeight
            Keys.onReturnPressed: {
                 inSite.enabled ? inSite.focus = true : inUser.focus=true;
            }
        }

        Button{

            text: "Get Icon from site"
            onClicked: crypt.getIconFromSite(inSite.text)
        }
        Image{
            source: strIcon
            width: 64
            height: 64
        }
      } /* end of col */
    }/* end of flickable */

    Component.onCompleted: {
        timer.stop()
    }

}
