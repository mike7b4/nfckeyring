import QtQuick 1.1
import com.nokia.meego 1.0



Page {
    orientationLock: PageOrientation.LockPortrait
        Column{
            id: column1
            anchors.fill: parent
            anchors.topMargin: 90
            spacing: 15

            Image{
                source: "qrc:/nfckeyring.png"
                height: 128
                width: 128
                fillMode: Image.PreserveAspectFit
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }

            }
//            anchors.centerIn: parent

            Label{
                text: "Nfc Keyring"
                anchors.horizontalCenter: parent.horizontalCenter
                platformStyle: LabelStyle
                {
                    fontPixelSize: 42
                }
            }

            Text {
                font.pixelSize: 32
                color: "white"
                text: "Version: "+version
                anchors.horizontalCenter: parent.horizontalCenter

            }
            Rectangle{
                gradient: Gradient {
                      GradientStop { position: 0.0; color: "#333333" }
                      GradientStop { position: 1.0; color: "#777777" }
                  }
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                height: 3
                width: parent.width-64
            }

            Text {
                width: 360
                font.pixelSize: 32
                color: "white"
                text: "<center>Copyright 2011-2013</center><center>Mikael Hermansson</center><center>with design help from</center><center>Sophie Wikström</center>"
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
                height: 200
            }

            Rectangle{
                gradient: Gradient {
                      GradientStop { position: 0.0; color: "#333333" }
                      GradientStop { position: 1.0; color: "#777777" }
                  }
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                height: 3
                width: parent.width-64
            }

            Text {
                width: parent.width-70
                font.pixelSize: 20
                color: "gray"
                text: "This app is licensed under GPL v2.0"
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignHCenter
                height: 200
                wrapMode: Text.WordWrap
            }

        }
        MouseArea{
            anchors.fill: parent
            onClicked: pageStack.pop()
        }
}
