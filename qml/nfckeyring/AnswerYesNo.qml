import QtQuick 1.1
import com.nokia.meego 1.0
Dialog {
   id: myDialog
   property string subtitle:"subtitle"
   property string texttitle:"title"
   title: Item{
      height: 50
      Label {
          id: text
          anchors.top: parent.top
          anchors.left: parent.left
          anchors.right: parent.right
         anchors.bottomMargin: 10
         font.pixelSize: 32
       //  color: "white"
         text: texttitle
         horizontalAlignment: Text.AlignHCenter
       }

       Rectangle {
         anchors.left: parent.left
         anchors.right: parent.right
         anchors.top: text.bottom
         id: titleField
         height: 2
         width: parent.width
         color: "white"
       }
    }
   content:Item {
     id: name
     height: 200
     width: parent.width
     Column {
         spacing: 5
         anchors.horizontalCenter: parent.horizontalCenter
         Label {
           anchors.bottomMargin: 10
           id: text2
       //    font.pixelSize: 32
//           anchors.centerIn: parent
       //    color: "white"
           text: subtitle
           horizontalAlignment: Text.AlignHCenter
         }

         Button {   text: "Yes";
                    onClicked:{
                        myDialog.accept()
                    }
                }

         Button {text: "No"; onClicked: myDialog.reject() }
       }
    }
}

