import QtQuick 1.1
import com.nokia.meego 1.0

Page{
    id: settingsPage

    tools: backTools
    anchors.margins: 16
    anchors.fill: parent
    orientationLock: PageOrientation.LockPortrait

    TopButton{
        id: topPanel
        anchors.top: parent.top
        textSubmit: "save"
        onSubmit:{
            var c=entryIp.text.split(".")
            if (c.length!=4)
            {
                labelError.color="red"
                label.text="STOP! Must be an IP<br />And only IPV4 adresses supported!"
            }
            else {
                strIp=entryIp.text
                iPort=entryPort.text
                configs.setValue("WallServer/IP",strIp)
                configs.setValue("WallServer/Port",iPort.toString())
                timer.restart()
                pageStack.pop();

            }
        }
        onCancel:
        {
           timer.restart()
           pageStack.pop()
        }
    }

    Column{
        id: col
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 32
        anchors.rightMargin: 32
//        width: parent.width
        spacing: 10
        anchors.top: topPanel.bottom

        Label {
            id: labelError
            color: "white"
            text: ""
            wrapMode: Text.WordWrap
        }

        Label{
            id: labelIp
            color: "white"
            text: "Your \"paste\" server IP:"
        }
        TextField{
            id: entryIp
/*            inputMask: "000.000.000.000;" */
            inputMethodHints: Qt.ImhFormattedNumbersOnly | Qt.ImhNoPredictiveText
            text: strIp
        }
        Label{
            id: labelPort
            color: "white"
            text: "Your \"paste\" server Port:"
        }
        TextField{
            id: entryPort
            validator: IntValidator{ bottom: 1; top: 65535; }
            inputMethodHints: Qt.ImhDigitsOnly | Qt.ImhNoPredictiveText
            text: iPort
        }
    }

    ToolBarLayout {
        id: backTools
        visible: true
        ToolIcon {
                 iconId: "toolbar-back";
                 onClicked: pageStack.pop()
        }
    }

}
