import QtQuick 1.1
import com.nokia.meego 1.0
import QtMobility.feedback 1.1
Button {
    width: 70
    height: 70
    HapticsEffect {
        id: vib
        attackIntensity: 0.0
        attackTime: 250
        intensity: 1.0
        duration: 100
        fadeTime: 250
        fadeIntensity: 0.0
    }
    onClicked: {
        vib.start();
        addText(text);
    }
}
