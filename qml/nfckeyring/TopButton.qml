import QtQuick 1.1
import com.nokia.meego 1.0
Item{
    id: root
    height: 70
    width: parent.width
    property string textSubmit: "ok"
    signal submit
    signal cancel
    Item {
        id: radTop
        height: 10
        width: parent.width
//        color: "darkgray"
        anchors.topMargin: 0
  //      radius: 10
    }
    Item {
        height: 63
        anchors.top: radTop.bottom-3
     //   color: "darkgray"
        width: parent.width
        Button {
            id: buttonCancel
            width: 100
            text: "Cancel"
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            anchors.leftMargin: 16
            onClicked: root.cancel()
        }
        Button{
            id: buttonSubmit
            width: 100
//            height: 60
            text: textSubmit
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.rightMargin: 16
            onClicked: root.submit()
        }
    }

}
