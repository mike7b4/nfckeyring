import QtQuick 1.1
import com.nokia.meego 1.0
import meeclipboard 1.0
Page {
    orientationLock: Screen.Portrait

    tools:  commonTools
    function getGroups()
    {
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?><groups><group><title>Banks</title></group><group><title>E-Mail accounts</title></group><group><title>Communitys</title></group><group><title>Forums</title></group><group><title>Other</title></group></groups>";
    }


    TopPanel{
        id: topPanel
    }

    ListView {
        id: listView
        spacing: 5
        width: parent.width
        height: mainPage.height-topPanel.height
        anchors.leftMargin: 16
        anchors.rightMargin: 16
        anchors.top: topPanel.bottom
        model: xmlModel
        delegate: GroupsDelegate {}
        focus: true
        currentIndex: -1
    }

    XmlListModel {
        id: xmlModel
        xml: getGroups()
        query: "/groups/group"

            XmlRole { name: "title"; query: "title/string()" }
    }


    ToolBarLayout {
        id: commonTools
        visible: false
        ToolIcon {
            id: buttonMenu
            platformIconId: "toolbar-view-menu"
            anchors.right: (parent === undefined) ? undefined : parent.right
            onClicked: (myMenu.status == DialogStatus.Closed) ? myMenu.open() : myMenu.close()
        }
        ToolIcon {
            id: buttonBack
                iconId: "toolbar-back";
                anchors.left: (parent === undefined) ? undefined : parent.left
            onClicked: { pageStack.pop(); }
        }
    }


}
