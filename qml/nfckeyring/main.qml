import QtQuick 1.1
import com.nokia.meego 1.0

PageStackWindow {
    id: appWindow

    initialPage: mainPage
    /* TODO: what selected group */
    property string version: "0.9.4"
    property string currentGroup: "forum"
    /* what selected site */
    property string strSite
    property string strPassword
    property string strGroup
    property string strUser
    property string strIcon
    property string strIp: ""
    property string strSNR: ""
    property int iPort: 80
    property string strNewPageTitle: "New"
    property bool bInSiteEnabled: false
    property string strMsg: "Login by Tap with your NFC Key, Buscard or Mifare compatible Tag"
    property string colorValue: "cyan"

    MainPage {
        id: mainPage
    }
    Timer {
        id: timer
        interval: 90000; running: false; repeat: false
        onTriggered: {
            strSNR=""
            mainPage.state=""
//            strMsg="Login by Tap with your NFC Key, Buscard or Mifare compatible Tag"
            pageStack.pop()
            if (pageStack.depth!=1)
                pageStack.pop()

            crypt.logout()
        }
    }

    ToolBarLayout {
        id: commonTools
        visible: true
        ToolIcon {
            visible: false
            iconId: "toolbar-backspace"
            anchors.left: (parent === undefined) ? undefined : parent.left
            onClicked: { strMsg="<b>Enter SNR or Tap a RFIDTag</b>"
                         colorValue="cyan"
                strSNR=""
                         crypt.logout()


                        }

        }
        ToolIcon {
            platformIconId: "toolbar-view-menu"
            anchors.right: (parent === undefined) ? undefined : parent.right
            onClicked: (myMenu.status == DialogStatus.Closed) ? myMenu.open() : myMenu.close()
        }
    }
/*
    platformStyle: PageStackWindowStyle {
             background: "file://opt/nfckeyring/images/stripes.png"
             backgroundFillMode: Image.Stretch
    }
    */
    platformStyle: PageStackWindowStyle {
         background: "image://theme/480x854"
         backgroundFillMode: Image.Stretch
     }

    Component.onCompleted: {
        theme.inverted = true;

    }

    Menu {
        id: myMenu
        visualParent: pageStack
        MenuLayout {
            MenuItem { text: qsTr("Configuration"); onClicked: { console.log("settings\n"); pageStack.push(Qt.resolvedUrl("Settings.qml"))} }
            MenuItem { text: qsTr("About Nfc Keyring"); onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml")) }
            MenuItem { text: qsTr("Mr Elop"); onClicked: pageStack.push(Qt.resolvedUrl("MrElop.qml")) }
        }
    }

    states: [
           State {
               name: "fullsize-visible"
               when: platformWindow.viewMode == WindowState.Fullsize && platformWindow.visible
               StateChangeScript {
                   script: {
                       /* not logged in? */
                       if (strSNR.length==0)
                           crypt.nfcActivate()
                       console.log("Visibility: Fullsize and visible!")
                   }
               }
           },
           State {
               name: "thumbnail-or-invisible"
               when: platformWindow.viewMode == WindowState.Thumbnail || !platformWindow.visible
               StateChangeScript {
                   script: {
                       crypt.nfcDeactivate()
                   }
               }
           }
       ]
}
