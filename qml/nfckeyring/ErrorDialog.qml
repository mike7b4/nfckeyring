
import QtQuick 1.1
import com.nokia.meego 1.0
Dialog {
   id: myDialog
   property string subtitle:"subtitle"
   property string texttitle:"title"
   title: Rectangle {
     id: titleField
     height: 2
     width: parent.width
     color: "white"
     Text {
        anchors.top: titleField.top
       anchors.bottomMargin: 10
       id: text
       height: 24
       font.pixelSize: 22
       color: "white"
       text: texttitle
     }
   }

   content:Item {
     id: name
     height: 400
     width: parent.width
         Text {
//           anchors.top: text.bottom
           height: 300
           anchors.bottomMargin: 10
           id: text2
           font.pixelSize: 16
           anchors.centerIn: parent
           color: "white"
           text: subtitle
         }

         Button {
             anchors.top: text2.bottom
             text: "Ok";
             onClicked: myDialog.reject()
         }
    }
}

