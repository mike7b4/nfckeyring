import QtQuick 1.1
Item{
    id: delegate
     height: column.height+30
     width: delegate.ListView.view.width
     Item {
        id: wrapper
        //radius: 5
         width: parent.width;
         height: parent.height;
         //color: "#333333"
//         color: ListView.isCurrentItem ? "lightsteelblue" : "lightgray"
         Column {
             id: column
             x: 20
             y: 20
             width: parent.width - 40

             Text {
                 id: idGroup
                 text: title
                 width: parent.width;
                 wrapMode: Text.WordWrap
                 color: "cyan"
                 font { bold: true;  pixelSize: 42 }
             }
             Rectangle{
                 gradient: Gradient {
                       GradientStop { position: 0.0; color: "#333333" }
                       GradientStop { position: 1.0; color: "#777777" }
                   }
                 anchors {
                     anchors.right: parent.right
                     anchors.left: parent.left
                     anchors.rightMargin: 16
                 }
                 height: 3
                 width: parent.width-16
             }

         }
         MouseArea{
             anchors.fill: parent
             onClicked:  {
                 /* those vars is used by NewPage*/
                currentGroup=title
                 pageStack.push(Qt.resolvedUrl("ListPasswords.qml"))
                 timer.restart()
             }

         }
     }

}
